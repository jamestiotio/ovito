.. _particles.modifiers.clear_selection:

Clear selection
---------------

This modifier clears the current selection of elements by removing the ``Selection`` property if it exists.
The :guilabel:`Operate on` field selects the kind of elements (particles, bonds, etc.) the modifier should act on.
See the introduction on :ref:`particle properties <usage.particle_properties>` to learn more about the role 
of the ``Selection`` property.

.. seealso::

  :py:class:`ovito.modifiers.ClearSelectionModifier` (Python API)